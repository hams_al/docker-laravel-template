# Docker-Laravel #

## 開発環境構築手順 ##
1. 「.env.example」をコピーし、「.env」を作成する
2. Dockerイメージを作成する
3. コンテナを起動する
4. ComposerでVendorファイルをインストールする
5. データベースのマイグレーションをする

※以下のコマンド類を参考にしてください

---

## コマンド類 ##

### Dockerイメージの作成 ###

※phpとmysqlがビルドされる
```
$ docker-compose build
```

※Xdebugを有効にするとき
```
$ docker-compose -f docker-compose.yml -f docker/options/enable-xdebug.yml build
```

### 作成したイメージの確認 ###

※dockerコマンドであることに注意
```
$ docker images
```


### コンテナの起動 ###

```
$ docker-compose up -d
```

### 起動確認 ###

```
$ docker-compose ps
```

### ブラウザから確認 ###

※mac環境でSSLの証明書エラーが出る場合は、httpで確認。

```
https://localhost/info.php
http://localhost:8080/
```


### 停止 ###

```
$ docker-compose stop
```

---

### Composerを使ってVendorファイルのインストール ###

※composer.json、composer.lockを確認
```
$ docker-compose exec web composer install
```

※Laravel初期画面表示
```
https://localhost/
```

---

### Migration ###
* マイグレーション
    ```
    $ docker-compose exec web php artisan migrate
    ```
* 初期値投入
    ```
    $ docker-compose exec web php artisan db:seed
    ```
* ロールバック
    ```
    $ docker-compose exec web php artisan migrate:rollback
    ```
* イニシャルファイル作成
    ```
    $ docker-compose exec web php artisan make:migration create_users_table
    ```
* 初期データファイル作成
    ```
    $ docker-compose exec web php artisan make:seeder UserSeeder
    ```

---

### PHPコンテナに入って環境変数を確認 ###

```
$ docker-compose exec web /bin/sh
# printenv APP_NAME
```

### MySQLコンテナに入ってdump取得 ###

```
$ docker-compose exec local-db /bin/sh
# mysqldump -u app_admin -p --single-transaction --databases app_db > /tmp/app_db.sql
```

---

### キャッシュクリア ###

```
$ docker-compose exec web php artisan cache:clear
$ docker-compose exec web php artisan config:clear
$ docker-compose exec web php artisan route:clear
$ docker-compose exec web php artisan view:clear
$ docker-compose exec web php artisan clear-compiled
$ docker-compose exec web php artisan optimize
```

---

### Laravelコマンドライン ###

```
$ docker-compose exec web php artisan tinker
```
---

### 再ビルド手順（作成した環境を初期化） ###

1. composerキャッシュクリア

    ```
    $ docker-compose exec web composer clear-cache
    ```

2. コンテナ、イメージ、ボリューム、ネットワークの削除

    ```
    $ docker-compose down --rmi all --volumes
    ```
   
3. 残りのイメージ削除

    ※他案件で使ってるイメージも削除されるので注意。
    無理に消す必要はないかも。。
    
    ```
    $ docker rmi -f $(docker images -aq)
    ```
    
    以下のコマンドでIMAGE IDを特定して消す方法も。  
    
    ```
    $ docker images
    
    ex)
    IMAGE ID
    b230c73cd1db
    add3b2118f18
    642ac79dc240
    
    $ docker rmi b230c73cd1db 642ac79dc240 -f
    ```

4. /template_app/vendorフォルダの削除

5. ビルド

---

### オプション指定のビルド ###

※XDEBUGを有効にするとき

```
docker-compose -f docker-compose.yml -f docker/options/enable-xdebug.yml build
```